+++
title = "Welcome!"
description = "A welcome to the Veloren development blog"

date = 2018-08-14
+++

# Welcome to Veloren!

Veloren is an open-world, open-source multiplayer voxel RPG.

Veloren currently in an early stage of development, but you can already play the game.

We're pleased to say we've finally created a development blog (hint: you're reading it). On this blog, we'll regularly put up articles discussion various parts of Veloren's development, new additions, and a weekly *This week in Veloren*  update.
